1. Зайшов у GitLub і створив репозиторій для 1- го ДЗ.
2. Відкрив VSCode і вставив силку на свій репозиторій і обрав новостворену папку (HomeWork1 Rep).
3. Перемістив файли 1-го ДЗ в новостворену папку.
4. Відкрив термінал і прописав дані команді:
-  git config --global user.name "Dmytro"
-  git config --global user.email "dimonchik1645@gmail.com"
-  git status
-  git add .
-  git commit -m "homework1"
-  git push
5. Відкрив свій репозиторій у GitLub і там з'явилася моє ДЗ.

